#pragma once

#include <kernie/kernie_special.h>

struct kernie_special_ppc : public kernie_special {
    static kernie_special_ppc the() {
        static kernie_special_ppc me;
        return me;
    }

    void init();
};