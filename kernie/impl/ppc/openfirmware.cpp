#include <kernie/impl/ppc/openfirmware.h>
#include <stdint.h>
#include <duktape.h>

namespace of {
    void exit() {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;
        } args = {
            "exit",
            0,
            0
        };

        of::openfirm(&args);
    }

    duk_ret_t JS_exit(duk_context *ctx) {
        exit();
        return 0;
    }

    uint32_t get_prop(uint32_t handle, const char *property, void *buffer, uint32_t buf_len) {
        static struct { 
            const char *cmd;
            int num_args;
            int num_returns;
            int handle;
            const char *prop;
            void *buffer;
            int buf_len;
            int size;
        } args = {
            "getprop",
            4,
            1
        };

        args.handle = handle;
        args.prop = property;
        args.buffer = buffer;
        args.buf_len = buf_len;

        if (of::openfirm(&args) == -1) return -1;

        return args.size;
    }

    // JS: of_get_prop(handle, property, buf_len)
    duk_ret_t JS_get_prop(duk_context *ctx) {
        // grab args off the JS stack
        int handle = duk_to_int(ctx, 0);
        const char *prop = duk_to_string(ctx, 1);
        int buf_len = duk_to_int(ctx, 2);

        // allocate our buffer that we'll put the result in and wrap
        void *buffer = malloc(buf_len);

        (void) get_prop(handle, prop, buffer, buf_len);

        // wrap our buffer and therefore return it
        duk_push_external_buffer(ctx);
        duk_config_buffer(ctx, -1, buffer, buf_len);

        return 1;
    }

    uint32_t prop_len(uint32_t handle, const char *property) {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;
            int handle;
            const char *property;
            int proplen;
        } args = {
            "getproplen",
            2,
            1
        };

        args.handle = handle;
        args.property = property;

        if (of::openfirm(&args) == -1) return -1;

        return args.proplen;
    }

    // JS: of_prop_len(handle, property) 
    duk_ret_t JS_prop_len(duk_context *ctx) {
        uint32_t handle = duk_to_int(ctx, 0);
        const char *property = duk_to_string(ctx, 1);
        
        duk_push_int(ctx, prop_len(handle, property));

        return 1;
    }

    uint32_t find_device(const char *name) {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;
            const char *device;
            int handle;
        } args = {
            "finddevice",
            1,
            1
        };

        args.device = name;

        if (of::openfirm(&args) == -1) return -1;

        return args.handle;
    }

    // JS: of_find_device(name)
    duk_ret_t JS_find_device(duk_context *ctx) {
        const char* name = duk_to_string(ctx, 0);
        duk_push_int(ctx, find_device(name));
        
        return 1;
    }

    uint32_t get_peer(uint32_t handle) {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;
            int handle;
            int sibling_handle;
        } args = {
            "peer",
            1,
            1
        };

        args.handle = handle;
        of::openfirm(&args);

        return args.sibling_handle;
    }

    // JS: of_get_peer(handle) 
    duk_ret_t JS_get_peer(duk_context *ctx) {
        uint32_t handle = duk_to_int(ctx, 0);
        duk_push_int(ctx, get_peer(handle));

        return 1;
    }

    uint32_t get_child(uint32_t handle) {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;
            int handle;
            int child_handle;
        } args = {
            "child",
            1,
            1
        };

        args.handle = handle;
        of::openfirm(&args);

        return args.child_handle;
    }

    // JS: of_get_child(handle) 
    duk_ret_t JS_get_child(duk_context *ctx) {
        uint32_t handle = duk_to_int(ctx, 0);
        duk_push_int(ctx, get_child(handle));

        return 1;
    }

    uint32_t get_parent(uint32_t handle) {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;
            int handle;
            int parent_handle;
        } args = {
            "parent",
            1,
            1
        };

        args.handle = handle;
        of::openfirm(&args);

        return args.parent_handle;
    }

    // JS: of_get_parent(handle)
    duk_ret_t JS_get_parent(duk_context *ctx) {
        uint32_t handle = duk_to_int(ctx, 0);
        duk_push_int(ctx, get_child(handle));

        return 1;
    }

    uint32_t next_prop(uint32_t handle, const char *previous, char* buffer) {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;
            int handle;
            const char *previous;
            char *buffer;
            int flag;
        } args = {
            "nextprop",
            3,
            1
        };

        args.handle = handle;
        args.previous = previous;
        args.buffer = buffer;

        of::openfirm(&args);

        return args.flag;
    }

    // JS: of_next_prop(handle, previous_prop_name)
    duk_ret_t JS_next_prop(duk_context *ctx) {
        uint32_t handle = duk_to_int(ctx, 0);
        const char* previous = duk_to_string(ctx, 1);

        char buffer[32];
        uint32_t flag = next_prop(handle, previous, buffer);

        duk_idx_t idx = duk_push_array(ctx);
        duk_push_string(ctx, buffer);
        duk_put_prop_index(ctx, idx, 0);
        duk_push_int(ctx, flag);
        duk_put_prop_index(ctx, idx, 1);

        return 1;
    }

    uint32_t package_to_path(uint32_t phandle, char *buffer, size_t buf_len) {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;
            uint32_t handle;
            char *buffer;
            size_t buf_len;
            uint32_t path_len;
        } args = {
            "package-to-path",
            3,
            1
        };

        args.handle = phandle;
        args.buffer = buffer;
        args.buf_len = buf_len;

        of::openfirm(&args);

        buffer[args.path_len] = '\0';

        return args.path_len;
    }

    // JS: of_package_to_path(phandle)
    duk_ret_t JS_package_to_path(duk_context *ctx) {
        uint32_t phandle = duk_to_int(ctx, 0);

        char buffer[255];
        (void) package_to_path(phandle, buffer, 255);

        duk_push_string(ctx, buffer);

        return 1;
    }

    uint32_t call_forth(const char* method, int handle, 
                int num_args, int num_returns, int *args_and_rets) {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;

            const char *method;
            int handle;
            int args_and_rets[12];
        } args = {
            "call-method",
            2,
            1
        };

        args.method = method;
        args.handle = handle;

        of::openfirm(&args);

        return 0;
    }

    uint32_t set_prop(uint32_t handle, const char *name, void *buffer, uint32_t buf_len) {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;

            uint32_t handle;
            const char *name;
            void *buffer;
            uint32_t buf_len;
            uint32_t size;
        } args = {
            "setprop",
            4,
            1
        };

        args.handle = handle;
        args.name = name;
        args.buffer = buffer;
        args.buf_len = buf_len;

        of::openfirm(&args);

        return args.size;
    }

    uint32_t get_fully_qualified_path(const char *device_specifier, char *buffer, uint32_t buf_len) {
        static struct {
            const char *cmd;
            int num_args; 
            int num_returns;

            const char *device_specifier;
            char* buffer;
            uint32_t buf_len;
            uint32_t length;
        } args = {
            "canon",
            3,
            1
        };

        args.device_specifier = device_specifier;
        args.buffer = buffer;
        args.buf_len = buf_len;

        of::openfirm(&args);

        return args.length;
    }

    // JS: of_get_fully_qualified_path(device_specifier)
    duk_ret_t JS_get_fully_qualified_path(duk_context *ctx) {
        const char *device_specifier = duk_to_string(ctx, 0);
        char buffer[255];
        
        uint32_t length = get_fully_qualified_path(device_specifier, buffer, 255);

        duk_idx_t arr = duk_push_array(ctx);
        duk_push_string(ctx, buffer);
        duk_put_prop_index(ctx, arr, 0);
        duk_push_int(ctx, length);
        duk_put_prop_index(ctx, arr, 1);

        return 1;
    }

    uint32_t open(const char* device) {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;
            const char *device_name;
            int handle;
        } args = {
            "open",
            1,
            1
        };

        args.device_name = device;
        if (of::openfirm(&args) == -1 || args.handle == 0) return -1;

        return args.handle;
    }

    // JS: of_open(device_name)
    duk_ret_t JS_open(duk_context *ctx) {
        const char *device = duk_to_string(ctx, 0);
        
        duk_push_int(ctx, open(device));

        return 1;
    }

    uint32_t write(uint32_t handle, char *buffer, uint32_t buf_len) {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;
            unsigned int handle;
            char* buffer;
            int length;
            int actual;
        } args = {
            "write",
            3,
            1
        };

        args.handle = handle;
        args.buffer = buffer;
        args.length = buf_len;
        args.actual = -1;

        of::openfirm(&args);
        return args.actual;
    }

    // JS: of_write(handle, buffer, buf_len)
    duk_ret_t JS_write(duk_context *ctx) {
        uint32_t handle = duk_to_int(ctx, 0);
        char *buffer = const_cast<char*>(duk_to_string(ctx, 1));
        uint32_t buf_len = duk_to_int(ctx, 2);

        duk_push_int(ctx, write(handle, buffer, buf_len));

        return 1;
    }

    void close(uint32_t handle) {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;

            uint32_t handle;
        } args = {
            "close",
            1,
            0
        };

        of::openfirm(&args);
    }

    // JS: of_close(handle)
    duk_ret_t JS_close(duk_context *ctx) {
        uint32_t handle = duk_to_int(ctx, 0);

        close(handle);

        return 0;
    }

    uint32_t read(uint32_t handle, void *buffer, uint32_t buf_len) {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;

            uint32_t handle;
            void *buffer;
            uint32_t buf_len;
            uint32_t actual;
        } args = {
            "read",
            3,
            1
        };

        of::openfirm(&args);

        return args.actual;
    }

    // JS: of_read(handle, size)
    duk_ret_t JS_read(duk_context *ctx) {
        uint32_t handle = duk_to_int(ctx, 0);
        uint32_t size = duk_to_int(ctx, 1);

        void* buffer = malloc(size);

        read(handle, buffer, size);

        duk_push_external_buffer(ctx);
        duk_config_buffer(ctx, -1, buffer, size);

        return 1;
    }

    uint32_t write(uint32_t handle, void *buffer, uint32_t buf_len) {
        static struct {
            const char *cmd;
            int num_args;
            int num_returns;

            uint32_t handle;
            void *buffer;
            uint32_t len;
            uint32_t actual;
        } args = {
            "write",
            3,
            1
        };

        args.handle = handle;
        args.buffer = buffer;
        args.len = buf_len;
        
        of::openfirm(&args);

        return args.actual;
    }

    uint32_t seek(uint32_t handle, uint32_t pos_hi, uint32_t pos_lo) {
        static struct {
            const char* cmd;
            int num_args;
            int num_returns;

            uint32_t handle;
            uint32_t pos_hi;
            uint32_t pos_lo;
            uint32_t status;
        } args = {
            "seek",
            3,
            1
        };

        args.handle = handle;
        args.pos_hi = pos_hi;
        args.pos_lo = pos_lo;
        
        of::openfirm(&args);

        return args.status;
    }

    // JS: of_seek(handle, hi, lo)
    duk_ret_t JS_seek(duk_context *ctx) {
        uint32_t handle = duk_to_int(ctx, 0);
        uint32_t hi = duk_to_int(ctx, 1);
        uint32_t lo = duk_to_int(ctx, 2);

        duk_push_int(ctx, seek(handle, hi, lo));

        return 1;
    }

    void *claim(void* virt, uint32_t size, uint32_t align) {
        static struct {
            const char *name;
            int num_args;
            int num_returns;
            void *virt;
            uint32_t size;
            uint32_t align;
            void *baseaddr;
        } args = {
            "claim",
            3,
            1,
        };

        args.virt = virt;
        args.size = size;
        args.align = align;

        of::openfirm(&args);

        return args.baseaddr;
    }

    // JS: of_claim(virt, size, align)
    duk_ret_t JS_claim(duk_context *ctx) {
        void* virt = (void*)duk_to_int(ctx, 0);
        uint32_t size = duk_to_int(ctx, 1);
        uint32_t align = duk_to_int(ctx, 2);

        void *buffer = claim(virt, size, align);

        duk_push_external_buffer(ctx);
        duk_config_buffer(ctx, -1, buffer, size);

        return 1;
    }
}