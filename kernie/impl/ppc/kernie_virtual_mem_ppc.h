#pragma once

#include <kernie/kernie_virtual_mem.h>

struct kernie_virtual_mem_ppc : public kernie_virtual_mem {
    static kernie_virtual_mem_ppc the() {
        static kernie_virtual_mem_ppc me;
        return me;
    }

    void init() {};
};