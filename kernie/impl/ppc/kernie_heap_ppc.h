#pragma once

#include <kernie/kernie_heap.h>

struct kernie_heap_ppc : public kernie_heap {
    static kernie_heap_ppc the() {
        static kernie_heap_ppc me;
        return me;
    }

    void init();
};