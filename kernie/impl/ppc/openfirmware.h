#pragma once

#include <stdint.h>
#include <duktape.h>

namespace of {
    extern int (*openfirm)(void*);
    extern int chosen;
    extern int stdout;
    extern int root;

    void *claim(void *virt, uint32_t size, uint32_t align);
    duk_ret_t JS_claim(duk_context *ctx);

    void release(void *virt, uint32_t size);

    uint32_t open(const char *device);
    duk_ret_t JS_open(duk_context *ctx);

    void close(uint32_t handle);
    duk_ret_t JS_close(duk_context *ctx);

    uint32_t read(uint32_t handle, void *buffer, uint32_t buf_len);
    duk_ret_t JS_read(duk_context *ctx);

    uint32_t seek(uint32_t handle, uint32_t pos_hi, uint32_t pos_lo);
    duk_ret_t JS_seek(duk_context *ctx);

    uint32_t write(uint32_t handle, char *buffer, uint32_t buf_len);

    uint32_t get_peer(uint32_t handle);
    duk_ret_t JS_get_peer(duk_context *ctx);

    uint32_t get_child(uint32_t handle);
    duk_ret_t JS_get_child(duk_context *ctx);

    uint32_t get_parent(uint32_t handle);
    duk_ret_t JS_get_parent(duk_context *ctx);

    uint32_t instance_to_package(uint32_t ihandle);

    uint32_t prop_len(uint32_t handle, const char *property);
    duk_ret_t JS_prop_len(duk_context *ctx);

    /*
        Retrieve a property from a device tree
        @returns how many bytes were read from the device path
        @param handle - handle to the device
        @param property - the name of the property
        @param buffer - the memory address to put the property in
        @param buf_len - the size of the buffer
    */
    uint32_t get_prop(uint32_t handle, const char *property, void *buffer, uint32_t buf_len);
    duk_ret_t JS_get_prop(duk_context *ctx);

    uint32_t next_prop(uint32_t handle, const char *previous, char* buffer);
    duk_ret_t JS_next_prop(duk_context *ctx);

    uint32_t set_prop(uint32_t handle, const char *name, void *buffer, uint32_t buf_len);

    uint32_t get_fully_qualified_path(const char *device_specifier, char *buffer, uint32_t buf_len);
    duk_ret_t JS_get_fully_qualified_path(duk_context *ctx);

    /*
        @returns pointer to an openfirmware device
        @param device - the path to the openfirmware device
    */
    uint32_t find_device(const char *device);
    duk_ret_t JS_find_device(duk_context *ctx);

    uint32_t instance_to_path(uint32_t ihandle, char *buffer, size_t buf_len);
    uint32_t package_to_path(uint32_t phandle, char *buffer, size_t buf_len);
    duk_ret_t JS_package_to_path(duk_context *ctx);

    // FIXME: this REALLY should be improved in the future to allow multiple return values (if we need it)
    //        -- also, we only allow 6 stack arguments, which is sort of ehh
    /*
        Calls a forth method (this is broken and doesn't actually work quite right)
        @returns the catch result. fix this please
        @param method - the name of the forth method
        @param handle - the device to call it on
        @param args_and_rets - a pointer to the args and return values
    */
    uint32_t call_forth(const char* method, int handle, int num_args, int num_returns, int *args_and_rets);

    void exit();
    duk_ret_t JS_exit(duk_context *ctx);
}