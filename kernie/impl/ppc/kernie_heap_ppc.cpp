#include <kernie/impl/ppc/kernie_heap_ppc.h>
#include <kernie/impl/ppc/openfirmware.h>

void kernie_heap_ppc::init() {
    this->heap_space = reinterpret_cast<unsigned char*>(of::claim(0, 1536 * 1024 * 1024, 2));
}