#pragma once

#include <kernie/kernie_interrupts.h>

struct kernie_interrupts_ppc : public kernie_interrupts {
    static kernie_interrupts_ppc the() {
        static kernie_interrupts_ppc me;
        return me;
    }

    void init() {};
};