#include <kernie/impl/ppc/kernie_heap_ppc.h>
#include <kernie/impl/ppc/kernie_virtual_mem_ppc.h>
#include <kernie/impl/ppc/kernie_processes_ppc.h>
#include <kernie/impl/ppc/kernie_interrupts_ppc.h>
#include <kernie/impl/ppc/kernie_special_ppc.h>