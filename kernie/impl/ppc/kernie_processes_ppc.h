#pragma once

#include <kernie/kernie_processes.h>

struct kernie_processes_ppc : public kernie_processes {
    static kernie_processes_ppc the() {
        static kernie_processes_ppc me;
        return me;
    }

    void init() {};
};