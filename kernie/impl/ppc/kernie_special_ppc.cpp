#include <kernie/impl/ppc/kernie_special_ppc.h>
#include <kernie/impl/ppc/openfirmware.h>

int of::chosen;
int of::stdout;
int of::root;

char *int2bin(int a, char *buffer, int buf_size) {
    buffer += (buf_size - 1);

    for (int i = 31; i >= 0; i--) {
        *buffer-- = (a & 1) + '0';

        a >>= 1;
    }

    return buffer;
}

inline uint32_t ppc_get_msr() {
	uint32_t msr;
	asm volatile("mfmsr %0" : "=r" (msr) );
	return msr;
}

inline void ppc_set_msr(uint32_t msr) {
	asm volatile("mtmsr %0" : : "r" (msr) );
}

struct msr_fields {
    union {
        uint32_t msr;
        struct {
            uint16_t dummy1 : 13;
            bool power_management : 1; // POW
            bool dummy2 : 1;
            bool exception_little_endian_mode : 1; // ILE
            bool external_interrupts : 1; // EE
            bool privilege : 1; // PR
            bool floating_point : 1; // FP
            bool machine_check : 1; // ME
            bool floating_point_exception_mode0 : 1; // FE0
            bool single_step_trace : 1; // SE
            bool branch_trace : 1; // BE
            bool floating_point_exception_mode1 : 1; // FE1
            bool dummy3 : 1;
            bool exception_prefix : 1; // IP
            bool instruction_address_translation : 1; // IR
            bool data_address_translation : 1; // DR
            uint16_t dummy4 : 2;
            bool recoverable_exception : 1; // RI
            bool little_endian : 1; // LE
        };
    };
};

void kernie_special_ppc::init() {
    // find the root device
    of::root = of::find_device("/");

    // find the /chosen device -- this holds all sorts of important information, including the pointers to stdin and stdout.
    of::chosen = of::find_device("/chosen");
    if (of::chosen == -1) {
        //panic("could not find /chosen!");
    }

    // if we've found /chosen successfully, we can get the "stdout" pointer from inside it.
    int size_of_stdout = of::get_prop(of::chosen, "stdout", &of::stdout, sizeof(of::stdout));
    if (size_of_stdout != sizeof(of::stdout)) {
        //panic("could not find /stdout!");
    }
    printf("stdout is at %d\r\n", of::stdout);

    // if it gives us a nullptr, we may be on a system that uses /screen instead (but this is a last-ditch attempt)
    if (of::stdout == 0) of::stdout = of::open("screen");

    // now read the MSR stuff
    uint32_t pre_msr = ppc_get_msr();
    char msr_bits[33];
    msr_bits[32] = '\0';
    int2bin(pre_msr, msr_bits, 32);
    printf("\tOF handed us this MSR: 0x%x %s!!\r\n", pre_msr, msr_bits);

    msr_fields msr = msr_fields{pre_msr};

    printf("\tbefore:\r\n");
    printf("\t\tpower management? %s\r\n", msr.power_management ? "enabled" : "disabled");
    printf("\t\texception little endian mode? %s\r\n", msr.exception_little_endian_mode ? "enabled" : "disabled");
    printf("\t\texternal interrupts? %s\r\n", msr.external_interrupts ? "enabled" : "disabled");
    printf("\t\trestricted privilege mode? %s\r\n", msr.privilege ? "enabled" : "disabled");
    printf("\t\tfloating point? %s\r\n", msr.floating_point ? "enabled" : "disabled");
    printf("\t\tmachine check? %s\r\n", msr.machine_check ? "enabled" : "disabled");
    printf("\t\tfloating point exception mode 0? %s\r\n", msr.floating_point_exception_mode0 ? "enabled" : "disabled");
    printf("\t\tsingle step trace? %s\r\n", msr.single_step_trace ? "enabled" : "disabled");
    printf("\t\tbranch trace? %s\r\n", msr.branch_trace ? "enabled" : "disabled");
    printf("\t\tfloating point exception mode 1? %s\r\n", msr.floating_point_exception_mode1 ? "enabled" : "disabled");
    printf("\t\texception prefix? %s\r\n", msr.exception_prefix ? "Fs" : "0s");
    printf("\t\tinstruction address translation? %s\r\n", msr.instruction_address_translation ? "enabled" : "disabled");
    printf("\t\tdata address translation? %s\r\n", msr.data_address_translation ? "enabled" : "disabled");
    printf("\t\trecoverable exception? %s\r\n", msr.recoverable_exception ? "enabled" : "disabled");
    printf("\t\tlittle endian? %s\r\n", msr.little_endian ? "enabled" : "disabled");

    msr.external_interrupts = true;
    ppc_set_msr(msr.msr);

    uint32_t post_msr = ppc_get_msr();
    msr_fields reset_msr = msr_fields{post_msr};

    printf("\tafter:\r\n");
    printf("\t\tpower management? %s\r\n", reset_msr.power_management ? "enabled" : "disabled");
    printf("\t\texception little endian mode? %s\r\n", reset_msr.exception_little_endian_mode ? "enabled" : "disabled");
    printf("\t\texternal interrupts? %s\r\n", reset_msr.external_interrupts ? "enabled" : "disabled");
    printf("\t\trestricted privilege mode? %s\r\n", reset_msr.privilege ? "enabled" : "disabled");
    printf("\t\tfloating point? %s\r\n", reset_msr.floating_point ? "enabled" : "disabled");
    printf("\t\tmachine check? %s\r\n", reset_msr.machine_check ? "enabled" : "disabled");
    printf("\t\tfloating point exception mode 0? %s\r\n", reset_msr.floating_point_exception_mode0 ? "enabled" : "disabled");
    printf("\t\tsingle step trace? %s\r\n", reset_msr.single_step_trace ? "enabled" : "disabled");
    printf("\t\tbranch trace? %s\r\n", reset_msr.branch_trace ? "enabled" : "disabled");
    printf("\t\tfloating point exception mode 1? %s\r\n", reset_msr.floating_point_exception_mode1 ? "enabled" : "disabled");
    printf("\t\texception prefix? %s\r\n", reset_msr.exception_prefix ? "Fs" : "0s");
    printf("\t\tinstruction address translation? %s\r\n", reset_msr.instruction_address_translation ? "enabled" : "disabled");
    printf("\t\tdata address translation? %s\r\n", reset_msr.data_address_translation ? "enabled" : "disabled");
    printf("\t\trecoverable exception? %s\r\n", reset_msr.recoverable_exception ? "enabled" : "disabled");
    printf("\t\tlittle endian? %s\r\n", reset_msr.little_endian ? "enabled" : "disabled");
}