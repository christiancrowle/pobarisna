#include <stdint.h>
#include <stdio.h>

#include <kernie/kernie_config.h>
#include <kernie/impl/ppc/openfirmware.h>

int (*of::openfirm)(void*);

void kmain() {
    kernie_special_impl::the().init();
    of::write(of::stdout, "special\r\n", 9);

    kernie_virtual_mem_impl::the().init();
    of::write(of::stdout, "virtual mem\r\n", 13);

    kernie_heap_impl::the().init();
    of::write(of::stdout, "heap\r\n", 6);

    kernie_interrupts_impl::the().init();
    of::write(of::stdout, "interrupts\r\n", 12);
    
    kernie_processes_impl::the().init();
    of::write(of::stdout, "processes\r\n", 11);

    while (true) {}
}

extern "C" void external_kmain(uint32_t r3, uint32_t r4, int (*openfw)(void*)) {
    of::openfirm = openfw;
    kmain();
}