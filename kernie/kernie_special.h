#pragma once

#include <kernie/kernie_module.h>

struct kernie_special : public kernie_module {
    void set_userdata(void* userdata) {
        this->userdata = userdata;
    }

    void* userdata;
};