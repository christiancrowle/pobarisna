#pragma once

struct kernie_module {
    virtual void init() = 0;
};