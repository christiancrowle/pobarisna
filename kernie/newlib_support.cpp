#include <errno.h>
#undef errno
extern int errno;

#include <sys/stat.h>
#include <sys/time.h>

#include <stdio.h>
#include <string.h>

#include <kernie/kernie_config.h>
#include <kernie/impl/ppc/openfirmware.h>

extern "C" {
    int close(int file) {
        // FIXME: stub
        return -1;
    }

    void _exit(int code) {
        // FIXME: stub
    }

    char *__env[1] = { 0 };
    char **environ = __env;

    int execve(char *name, char **argv, char **env) {
        // FIXME: stub
        errno = ENOMEM;
        return -1;
    }

    int fork() {
        // FIXME: stub
        errno = EAGAIN;
        return -1;
    }

    int fstat(int file, struct stat *st) {
        // FIXME: stub
        st->st_mode = S_IFCHR;
        return 0;
    }

    int getpid() {
        // FIXME: stub
        return 1;
    }

    int isatty(int file) {
        // FIXME: stub
        return 1;
    }

    int kill(int pid, int sig) {
        // FIXME: stub
        errno = EINVAL;
        return -1;
    }

    int link(char *old, char *n) {
        // FIXME: stub
        errno = EMLINK;
        return -1;
    }

    int lseek(int file, int ptr, int dir) {
        // FIXME: stub
        return 0;
    }

    int open(const char *name, int flags, int mode) {
        // FIXME: stub
        return -1;
    }

    int read(int file, char *ptr, int len) {
        // FIXME: stub
        return 0;
    }

    void* sbrk(int incr) {
        // FIXME: this is an intentional stub
        return 0;
    }

    void *malloc(size_t size) {
        return kernie_heap_impl::the().malloc(size);
    }

    void *realloc(void *ptr, size_t size) {
        return kernie_heap_impl::the().realloc(ptr, size);
    }

    void free(void *ptr) {
        kernie_heap_impl::the().free(ptr);
    }

    int times(struct tms *buf) {
        // FIXME: stub
        return -1;
    }

    int unlink(char *name) {
        // FIXME: stub
        errno = ENOENT;
        return -1; 
    }

    int wait(int *status) {
        // FIXME: stub
        errno = ECHILD;
        return -1;
    }

    int write(int file, char *ptr, int len) {
        if (file == 1) file = of::stdout;
        return of::write(file, ptr, len);
    }

    int gettimeofday(struct timeval *__restrict tv, void *__restrict tz) {
        // FIXME: stub
        tv->tv_sec = 60475;
        tv->tv_usec = 60475 * 1000000;

        return 0;
    }
}