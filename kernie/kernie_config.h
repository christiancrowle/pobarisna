#pragma once

#include <kernie/impl/ppc/impl.h>

#define kernie_heap_impl kernie_heap_ppc
#define kernie_virtual_mem_impl kernie_virtual_mem_ppc
#define kernie_processes_impl kernie_processes_ppc
#define kernie_interrupts_impl kernie_interrupts_ppc
#define kernie_special_impl kernie_special_ppc