#pragma once

#include <stdint.h>
#include <stdarg.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

int printf (const char *s, ...);
int puts (const char *s);

int vprintf(const char *__restrict format, va_list ap);
int vdprintf(int fd, const char *__restrict format, va_list ap);
int vsprintf(char *__restrict str, const char *__restrict format, va_list ap);
int vsnprintf(char *__restrict str, size_t size, const char *__restrict format, va_list ap);

int dprintf(int fd, const char *__restrict format, ...);
int sprintf(char *__restrict str, const char *__restrict format, ...);
int snprintf(char *__restrict str, size_t size, const char *__restrict format, ...);

int scanf(const char *__restrict format, ...);
int sscanf(const char *__restrict str, const char *__restrict format, ...);

int vscanf(const char *__restrict format, va_list ap);
int vsscanf(const char *__restrict str, const char *__restrict format, va_list ap);

#ifdef __cplusplus
}
#endif