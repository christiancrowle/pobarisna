#!/usr/bin/env python3
import sys
import socket

host_n_port = ("192.168.1.194", 19084)

file_mode = False
if sys.argv[1]:
    file_mode = True
    with open(sys.argv[1], "r") as f:
        file_data = f.read()

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(host_n_port)
    sock.listen()
    connection, addr = sock.accept()
    with connection:
        print("connected. waiting for ack")
        ack = connection.recv(4)
        print(f"ACK: {ack}")
        if file_mode:
            connection.sendall(file_data.encode("utf-8"))
        
        while not file_mode:
            data = input("> ").encode("utf-8")
            if data:
                connection.sendall(data)
            if data == b"quit":
                break