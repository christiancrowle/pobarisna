# Pics of Bread's Awful scRipting Interpreter System for Neat Architectures

It's like a microkernel, but bad, and all the servers are written in much weirder, more poorly suited languages than usual.

## Notes

You'll need a `powerpc-elf` GCC cross toolchain. This should be fairly easy to compile. 

This project uses [Rake](https://github.com/ruby/rake) for its build system. So you'll need that, too. To run the TFTP server, you'll also need inetd TFTP installed. If you're using the dongle or the bootstrapper, you'll need python installed too (more docs to come soon on how to build a dongle or set up the bootstrapper).

To compile and run the tftp server, just

`$ rake run-tftp`

and in a few moments you should have a TFTP server running. Run `boot enet:<your dev machine ip>,\ppc\load_kernie` in an OF shell to boot.

Otherwise, if you're using the bootstrapper,

`$ rake run-dongle`

and POBARISNA should boot, outputting to your terminal over telnet.

Note that the `dev` branch will almost never work. `main` will always contain a working version of the code.


## License

This code is under the MIT license. See LICENSE for more details.

`bootloader/crt0.S` was originally written for the L4Ka project, and it's under the BSD license. See `bootloader/INFO` for more information.
