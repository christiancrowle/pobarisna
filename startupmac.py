#!/usr/bin/env python3

import serial
import sys
import time

ser = serial.Serial(sys.argv[1], 9600, timeout=1)
print(f"name: {ser.name}")
time.sleep(int(sys.argv[2]))

#print("turning off")
#ser.write(b"3\n")
#ack = ser.readline()
#print("command sent")

#time.sleep(int(sys.argv[3]))
print("turning on")
ser.write(b"2\n")

ack = ser.readline()
ser.close()
