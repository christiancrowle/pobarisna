import os
import urllib.request

# mount the bootstrap
os.system("mount /dev/root -oremount,rw")
os.system("mount /dev/sda4 /mnt")

# download the POBARISNA kernie
urllib.request.urlretrieve("http://192.168.1.13/load_kernie", "/mnt/load_kernie")

# write out our boot script
with open("/mnt/boot_kernie.txt", "w") as boot_kernie_script:
    boot_kernie_script.write("""\ boot up kernie
cr

setenv boot-device hd:2,\yaboot
sync-nvram

dev /packages/telnet
" enet:telnet,192.168.1.250" io

boot hd:4,\load_kernie
""")

# unmount
os.system("busybox umount /mnt")

# update the nvram (I HATE string escapes!)
os.system(r"nvram -p common --update-config boot-device=hd:4,\\boot_kernie.txt")

# reboot!
os.system("reboot")