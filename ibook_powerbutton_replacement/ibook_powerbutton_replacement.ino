void setup() {
  // put your setup code here, to run once:
  pinMode(7, OUTPUT);
  Serial.begin(9600);
}

int do_it;
void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available()) {
    do_it = Serial.parseInt();
    do_it = constrain(do_it, 0, 255);
    //Serial.println(do_it);
    if (do_it == 2) {
      digitalWrite(7, HIGH);
      delay(500);
      digitalWrite(7, LOW);
      Serial.println("ACKON");
      do_it = 0;
    } else if (do_it == 3) {
      digitalWrite(7, HIGH);
      delay(5000);
      digitalWrite(7, LOW);
      Serial.println("ACKOFF");
      do_it = 0;
    }
  }
}
