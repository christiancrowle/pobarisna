#pragma once

#include <string.h>
#include <stdint.h>

#include "linked_list.h"

namespace handy::data {
    class string {
    private:
        char* _string;
    public:
        string() {
            _string = new char[1];
            _string[0] = '\0';
        };
        ~string() {
            delete _string;
        }

        string(char* src) {
            _string = new char[strlen(src)];
            strcpy(_string, src);
        };

        string(string& src) {
            size_t size = strlen(src.raw()) + 1;
            _string = new char[size];
            strcpy(_string, src.raw());
        }

        char* raw() {
            return _string;
        }

        char at(int idx) {
            return _string[idx];
        }

        void append(string& src) {
            size_t size = strlen(src.raw()) + strlen(_string) + 1;

            char* new_str = new char[size];
            strcpy(new_str, _string);
            strcat(new_str, src.raw());

            delete _string;
            _string = new_str;
        }

        size_t len() {
            return strlen(_string);
        }

        void copy_from_raw(char* source) {
            size_t src_len = strlen(source);
            if (src_len > len()) {
                delete _string;
                _string = new char[strlen(source)];
            }
            
            strcpy(_string, source);
        }

        void copy_from_raw(char* source, int n) {
            size_t src_len = strlen(source);
            if (src_len > len()) {
                delete _string;
                _string = new char[strlen(source)];
            }
            
            strncpy(_string, source, n);
        }

        // splits a string at the first delimiter, returning the string on both sides of the delimiter
        handy::data::linked_list<handy::data::string> split_first(char delimiter) {
            handy::data::string first;
            handy::data::string second;

            for (int i = 0; i < len(); i++) {
                if (at(i) == delimiter) {
                    first.copy_from_raw(raw(), i-1);
                    second.copy_from_raw(raw() + i+1, len() - i);

                    handy::data::linked_list<handy::data::string> ret;
                    ret.add(first);
                    ret.add(second);

                    return ret;
                }
            }

            return handy::data::linked_list<handy::data::string>();
        }
    };
}