#pragma once

namespace handy::data {
    template <typename T>
    class node {
    public:
        T contents;
        node* next;
    };

    template <typename T>
    class linked_list {
    private:
        node<T> *head;
        node<T> *tail;
        node<T> *get_node(int position) {     
            node<T> *current_node = head;       
            for (int i = 0; i < count(); i++) {
                if (i == position) return current_node;
                current_node = current_node->next;
            }

            return nullptr;
        }
    public:
        linked_list() : head{nullptr}, tail{nullptr} {}
        void add(T contents) {
            node<T> *new_node = new node<T>;
            new_node->contents = contents;
            new_node->next = nullptr;

            if (!head) {
                head = new_node;
                tail = new_node;
            } else {
                tail->next = new_node;
                tail = tail->next;
            }
        }

        int count() {
            node<T> *current_node = head;
            int count = 0;

            while (current_node) {
                count++;
                current_node = current_node->next;
            }

            return count;
        }

        T get(int position) {     
            node<T> *current_node = head;       
            for (int i = 0; i < count(); i++) {
                if (i == position) return current_node->contents;
                current_node = current_node->next;
            }

            return T();
        }

        void del(int position) {
            node<T> *current_node = head;
            node<T> *node_to_retarget;
            node<T> *node_to_delete;

            int nodes_count = count();

            // retarget the node
            for (int i = 0; i < nodes_count; i++) {
                if (i == position - 1) node_to_retarget = get_node(i); // save the node to retarget (the node before the one to del)
                if (i == position) node_to_delete = node_to_retarget->next; // save the node we want to del
                if (i == position + 1) {
                    // now skip over the node we want to del, then delete the node
                    node_to_retarget->next = get_node(i);
                    delete node_to_delete;
                    return;
                }
            }
        }

        void each(void (*todo)(T, int)) {
            for (int i = 0; i < count(); i++) {
                todo(get(i), i);
            }
        }

        T search(bool (*predicate)(T)) {
            for (int i = 0; i < count(); i++) {
                T ret = get(i);
                if (predicate(ret)) {
                    return ret;
                }
            }
        }
    };
}