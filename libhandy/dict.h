#pragma once

#include <libhandy/linked_list.h>

namespace handy::data {
    template<typename T, typename Y>
    struct pair {
        T first;
        Y second;
    };

    template<typename T, typename Y>
    class dictionary {
    public:
        Y operator [](T key) {
            return get(key);
        }

        Y get(T key) {
            for (int i = 0; i < pairs.count(); i++) {
                pair<T,Y> p = pairs.get(i);
                if (p.first == key) {
                    return p.second;
                }
            }
        }

        void append(T key, Y value) {
            pair<T, Y> p;
            p.first = key;
            p.second = value;
            pairs.add(p);
        }

        void append(T key) {
            pair<T, Y> p;
            p.first = key;
            pairs.add(p);
        }
    private:
        linked_list<pair<T, Y>> pairs;
    };
}