#pragma once

#include "linked_list.h"

namespace handy::data {
    template<typename T>
    class queue {
    public:
        void push(T item) {
            backend.add(item);
        }

        T pop() {
            T res = backend.get(0);
            backend.del(0);
            return res;
        }

        size_t length() {
            return backend.count();
        }
    private:
        linked_list<T> backend;
    };
}